{-# LANGUAGE LambdaCase, RankNTypes #-}

module Main (main) where

import Control.Monad (forever, void)
import Control.Concurrent (threadDelay, forkIO)
import Control.Monad.Trans.Reader (ReaderT)
import Data.List (isPrefixOf)
import Data.Time.Clock (getCurrentTime, addUTCTime, diffUTCTime)
import SignalDBus

import ParseMessage
import Persist

magicWord :: String
magicWord = "remindme"

-- |Try parsing a reminder from a received message. May send errors on signal.
getReminder :: SignalConn -> ReceivedMessage -> IO (Maybe Reminder)
getReminder sc msg@(Message ts n g m _) | magicWord `isPrefixOf` m = do
    case parseMsg (drop (length magicWord) m) of
        Left err -> do
            reactTo msg "❌" False sc
            replyTo msg ("Didn't understand that: " ++ show err) [] sc
            return Nothing
        Right (t, remText) -> do
            reactTo msg "✅" False sc
            case t of
                Left off -> return $ Just $ Reminder n g remText
                    $ fromIntegral off `addUTCTime` toUTCTime ts
                Right date -> return $ Just $ Reminder n g remText date
getReminder _ _ = return Nothing

-- |Waits, sends the reminder, and then deletes it from the database.
setReminder :: (forall a. ReaderT SqlBackend IO a -> IO a) -> Entity Reminder -> IO ()
setReminder runSql remEnt = void $ forkIO $ do
    let (Reminder n g remText at) = entityVal remEnt
    t <- getCurrentTime
    threadDelay $ 1000000 * round (at `diffUTCTime` t)

    withConn $ \sc -> case g of
        Nothing -> sendMessage ("Reminder: " ++ remText) [] [n] sc
        Just gId -> sendGroupMessage ("Reminder: " ++ remText) [] gId sc

    runSql $ delete $ entityKey remEnt

main :: IO ()
main = runRemindBackend $ \runSql -> withConn $ \sc -> do
        selfNum <- getSelfNumber sc
        putStrLn $ "Running on account: " ++ show selfNum

        pendingRems <- runSql $ selectList [] []
        mapM_ (setReminder runSql) pendingRems

        withReceiveMessages sc $ \getMsg -> forever $ getMsg >>= getReminder sc >>= \case
            Nothing -> return ()
            Just reminder -> do
                k <- runSql $ insert reminder
                setReminder runSql $ Entity k reminder
