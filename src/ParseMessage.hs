{-# LANGUAGE FlexibleContexts #-}

module ParseMessage (parseMsg) where

import Data.Functor.Identity (Identity)
import Data.Char (digitToInt)
import Data.List (foldl')
import Data.Time.Clock (UTCTime)
import Data.Time.Format (parseTimeM, defaultTimeLocale)
import Text.Parsec

parseMsg :: String -> Either ParseError (Either Int UTCTime, String)
parseMsg m = parse (spaces >> parser) "" m

parseInt :: Stream s Identity Char => Parsec s u Int
parseInt = foldl' (\a i -> a * 10 + digitToInt i) 0 <$> many1 digit

parseInterval :: Stream s Identity Char => Parsec s u Int
parseInterval = do
    n <- parseInt
    fmt <- many $ satisfy (/= ' ')
    spaces
    case fmt of
        ""  -> return n
        "m" -> return $ n * 60
        "h" -> return $ n * 3600
        "d" -> return $ n * 3600 * 24
        "w" -> return $ n * 3600 * 24 * 7
        _ -> fail $ "Didn't recognize format: " ++ fmt

parseDate :: Stream s Identity Char => Parsec s u UTCTime
parseDate = do
    spaces
    string "at"
    spaces
    dayStr  <- manyTill anyChar space
    spaces
    timeStr <- manyTill anyChar space
    spaces
    parseTimeM False defaultTimeLocale "%-d.%-m.%Y %H:%M" $ dayStr ++ " " ++ timeStr

parser :: Parsec String u (Either Int UTCTime, String)
parser = (,)
    <$> ((Left . sum <$> many1 parseInterval) <|> (Right <$> parseDate))
    <*> getInput
