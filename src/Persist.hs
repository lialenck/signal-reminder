{-# LANGUAGE EmptyDataDecls, FlexibleContexts, GADTs, GeneralizedNewtypeDeriving,
             MultiParamTypeClasses, OverloadedStrings, QuasiQuotes, TemplateHaskell,
             TypeFamilies, DerivingStrategies, UndecidableInstances, DataKinds,
             StandaloneDeriving, FlexibleInstances, RankNTypes #-}

{-# OPTIONS_GHC -Wno-orphans -Wno-name-shadowing #-}

module Persist (
    Reminder(..),
    runRemindBackend,
    getNextReminder,

    module Database.Persist,
    SqlBackend,
) where

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Logger (runStderrLoggingT, LogLevel(LevelDebug), filterLogger)
import Control.Monad.Trans.Reader (ReaderT, runReaderT)

-- db stuff
import Database.Persist
import Database.Persist.Sqlite
import Database.Persist.TH
import Data.Pool (withResource)

-- extra types & functionality
import Data.Time.Clock
import qualified Data.Text as T

-- internal stuff required for the 'PersistField Group' instance
import DBus.Internal.Types
import SignalDBus.Types

instance PersistField Group where
    toPersistValue (Group (ObjectPath s)) = PersistText $! T.pack s

    fromPersistValue (PersistText s) = Right $! Group $ ObjectPath $ T.unpack s
    fromPersistValue _ = Left $ T.pack "Cannot convert to Group"

instance PersistFieldSql Group where
    sqlType _ = SqlString

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Reminder
    number String
    group Group Maybe
    body String
    date UTCTime
    deriving Show
|]

runRemindBackend :: ((forall a. ReaderT SqlBackend IO a -> IO a) -> IO b) -> IO b
runRemindBackend f = runStderrLoggingT $ filterLogger (\_ ll -> ll > LevelDebug) $
  withSqlitePool "pending-reminders.db" 2 $ \pool -> do

    let runSql :: ReaderT SqlBackend IO a -> IO a
        runSql act = withResource pool $ \b -> runReaderT act b

    liftIO $ runSql $ runMigration migrateAll
    liftIO $ f runSql

-- couldn't figure out how to export 'ReminderDate' lol
getNextReminder :: ReaderT SqlBackend IO (Maybe (Entity Reminder))
getNextReminder = selectFirst [] [Asc ReminderDate]
